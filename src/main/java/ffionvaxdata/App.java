package ffionvaxdata;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public class App 
{
    public static void main( String[] args ) throws IOException, InterruptedException {
        
        // use the callout_class to get a HttpResponse<String>
        callout callout_instance = new callout();
        HttpResponse<String> response = callout_instance.new_callout();

        // Instantiate the parse_JSON class
        parse_JSON parse_JSON_instance = new parse_JSON();

        // use the parse_JSON instance to create objects we will use to readValue
        ObjectMapper obj_ObjectMapper = parse_JSON_instance.newObjectMapper();
        // because I haven't defined all of the attributes in the bean, need to set this configuration so parsing won't fail
        obj_ObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        vaccination_Bean obj_vaccination_Bean = parse_JSON_instance.newVaccination_Bean();

        // readValue of JSON. **We are deserializing the body of the response, instead of the response itself
        //obj_vaccination_Bean = obj_ObjectMapper.readValue(response.body(), vaccination_Bean.class);

        // use the getter/setter methods from catfacts_Bean to print out different attributes
        // System.out.println("Country: " + obj_vaccination_Bean.getCountry());
        
        List<vaccination_Bean> vaccinationList = obj_ObjectMapper.readValue(response.body(), new TypeReference<List<vaccination_Bean>>() {});

        vaccination_Bean first_in_list = (vaccinationList.get(0));

        System.out.println(first_in_list);


        System.out.println(first_in_list.getData(0));

        // get to the first object in the list of data for first country, then print
        String countryData = first_in_list.getDate();

        System.out.println(countryData);

    }
}
