package ffionvaxdata;

import java.net.http.*;
import java.net.http.HttpResponse.BodyHandlers;
import java.io.IOException;
import java.net.URI;

public class callout{

    public HttpResponse<String> new_callout() throws IOException, InterruptedException{
    // instantiate new HttpClient
    HttpClient client = HttpClient.newHttpClient();

    // build the request
    HttpRequest req = HttpRequest.newBuilder().uri(URI.create("https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/vaccinations.json")).build();

    // instantiate and return HttpResponse object after sending request. HttpResponse object cannot be raw. BodyHandlers.ofString tells server we want to receive the request as a string
    HttpResponse<String> response = client.send(req,BodyHandlers.ofString());

    return response;
    
    }

}