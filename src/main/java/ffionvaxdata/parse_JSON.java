package ffionvaxdata;

import com.fasterxml.jackson.databind.ObjectMapper;

// The point of this class is to create methods to instantiate ObjectMapper and catfacts_Bean

public class parse_JSON{

    // method to instantiate an ObjectMapper
    ObjectMapper newObjectMapper(){

        ObjectMapper defaultObjectMapper = new ObjectMapper();

        return defaultObjectMapper;
    }

    // instantiate an ObjectMapper using the method
    ObjectMapper obj_ObjectMapper = newObjectMapper();

    // method to instantiate a catfacts_Bean
    public vaccination_Bean newVaccination_Bean(){

        vaccination_Bean defaultVaccination_Bean = new vaccination_Bean();

        return defaultVaccination_Bean;
    }

    //instantiate a catfacts_Bean using the method
    vaccination_Bean obj_Catfacts_Bean = newVaccination_Bean();
    
}