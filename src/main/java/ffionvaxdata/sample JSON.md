[
  {
    "country": "Afghanistan",
    "iso_code": "AFG",
    "data": [
      {
        "date": "2021-02-22",
        "total_vaccinations": 0,
        "people_vaccinated": 0,
        "total_vaccinations_per_hundred": 0.0,
        "people_vaccinated_per_hundred": 0.0
      },
      {
        "date": "2021-02-23",
        "daily_vaccinations": 1367,
        "daily_vaccinations_per_million": 34,
        "daily_people_vaccinated": 1367,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-02-24",
        "daily_vaccinations": 1367,
        "daily_vaccinations_per_million": 34,
        "daily_people_vaccinated": 1367,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-02-25",
        "daily_vaccinations": 1367,
        "daily_vaccinations_per_million": 34,
        "daily_people_vaccinated": 1367,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-02-26",
        "daily_vaccinations": 1367,
        "daily_vaccinations_per_million": 34,
        "daily_people_vaccinated": 1367,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-02-27",
        "daily_vaccinations": 1367,
        "daily_vaccinations_per_million": 34,
        "daily_people_vaccinated": 1367,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-02-28",
        "total_vaccinations": 8200,
        "people_vaccinated": 8200,
        "daily_vaccinations": 1367,
        "total_vaccinations_per_hundred": 0.02,
        "people_vaccinated_per_hundred": 0.02,
        "daily_vaccinations_per_million": 34,
        "daily_people_vaccinated": 1367,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-03-01",
        "daily_vaccinations": 1580,
        "daily_vaccinations_per_million": 40,
        "daily_people_vaccinated": 1580,
        "daily_people_vaccinated_per_hundred": 0.004
      },
      {
        "date": "2021-03-02",
        "daily_vaccinations": 1794,
        "daily_vaccinations_per_million": 45,
        "daily_people_vaccinated": 1794,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-03-03",
        "daily_vaccinations": 2008,
        "daily_vaccinations_per_million": 50,
        "daily_people_vaccinated": 2008,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-03-04",
        "daily_vaccinations": 2221,
        "daily_vaccinations_per_million": 56,
        "daily_people_vaccinated": 2221,
        "daily_people_vaccinated_per_hundred": 0.006
      },
      {
        "date": "2021-03-05",
        "daily_vaccinations": 2435,
        "daily_vaccinations_per_million": 61,
        "daily_people_vaccinated": 2435,
        "daily_people_vaccinated_per_hundred": 0.006
      },
      {
        "date": "2021-03-06",
        "daily_vaccinations": 2649,
        "daily_vaccinations_per_million": 66,
        "daily_people_vaccinated": 2649,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-07",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-08",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-09",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-10",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-11",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-12",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-13",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-14",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-15",
        "daily_vaccinations": 2862,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-16",
        "total_vaccinations": 54000,
        "people_vaccinated": 54000,
        "daily_vaccinations": 2862,
        "total_vaccinations_per_hundred": 0.14,
        "people_vaccinated_per_hundred": 0.14,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2862,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-17",
        "daily_vaccinations": 2882,
        "daily_vaccinations_per_million": 72,
        "daily_people_vaccinated": 2882,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-18",
        "daily_vaccinations": 2902,
        "daily_vaccinations_per_million": 73,
        "daily_people_vaccinated": 2902,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-19",
        "daily_vaccinations": 2921,
        "daily_vaccinations_per_million": 73,
        "daily_people_vaccinated": 2921,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-20",
        "daily_vaccinations": 2941,
        "daily_vaccinations_per_million": 74,
        "daily_people_vaccinated": 2941,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-21",
        "daily_vaccinations": 2961,
        "daily_vaccinations_per_million": 74,
        "daily_people_vaccinated": 2961,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-22",
        "daily_vaccinations": 2980,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 2980,
        "daily_people_vaccinated_per_hundred": 0.007
      },
      {
        "date": "2021-03-23",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-03-24",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-03-25",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-03-26",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-03-27",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-03-28",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-03-29",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-03-30",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-03-31",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-04-01",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-04-02",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-04-03",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-04-04",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-04-05",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-04-06",
        "daily_vaccinations": 3000,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-04-07",
        "total_vaccinations": 120000,
        "people_vaccinated": 120000,
        "daily_vaccinations": 3000,
        "total_vaccinations_per_hundred": 0.3,
        "people_vaccinated_per_hundred": 0.3,
        "daily_vaccinations_per_million": 75,
        "daily_people_vaccinated": 3000,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-04-08",
        "daily_vaccinations": 3714,
        "daily_vaccinations_per_million": 93,
        "daily_people_vaccinated": 3714,
        "daily_people_vaccinated_per_hundred": 0.009
      },
      {
        "date": "2021-04-09",
        "daily_vaccinations": 4429,
        "daily_vaccinations_per_million": 111,
        "daily_people_vaccinated": 4429,
        "daily_people_vaccinated_per_hundred": 0.011
      },
      {
        "date": "2021-04-10",
        "daily_vaccinations": 5143,
        "daily_vaccinations_per_million": 129,
        "daily_people_vaccinated": 5143,
        "daily_people_vaccinated_per_hundred": 0.013
      },
      {
        "date": "2021-04-11",
        "daily_vaccinations": 5857,
        "daily_vaccinations_per_million": 147,
        "daily_people_vaccinated": 5857,
        "daily_people_vaccinated_per_hundred": 0.015
      },
      {
        "date": "2021-04-12",
        "daily_vaccinations": 6571,
        "daily_vaccinations_per_million": 165,
        "daily_people_vaccinated": 6571,
        "daily_people_vaccinated_per_hundred": 0.016
      },
      {
        "date": "2021-04-13",
        "daily_vaccinations": 7286,
        "daily_vaccinations_per_million": 183,
        "daily_people_vaccinated": 7286,
        "daily_people_vaccinated_per_hundred": 0.018
      },
      {
        "date": "2021-04-14",
        "daily_vaccinations": 8000,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-15",
        "daily_vaccinations": 8000,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-16",
        "daily_vaccinations": 8000,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-17",
        "daily_vaccinations": 8000,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-18",
        "daily_vaccinations": 8000,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-19",
        "daily_vaccinations": 8000,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-20",
        "daily_vaccinations": 8000,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-21",
        "daily_vaccinations": 8000,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-22",
        "total_vaccinations": 240000,
        "people_vaccinated": 240000,
        "daily_vaccinations": 8000,
        "total_vaccinations_per_hundred": 0.6,
        "people_vaccinated_per_hundred": 0.6,
        "daily_vaccinations_per_million": 201,
        "daily_people_vaccinated": 8000,
        "daily_people_vaccinated_per_hundred": 0.02
      },
      {
        "date": "2021-04-23",
        "daily_vaccinations": 8846,
        "daily_vaccinations_per_million": 222,
        "daily_people_vaccinated": 8428,
        "daily_people_vaccinated_per_hundred": 0.021
      },
      {
        "date": "2021-04-24",
        "daily_vaccinations": 9692,
        "daily_vaccinations_per_million": 243,
        "daily_people_vaccinated": 8855,
        "daily_people_vaccinated_per_hundred": 0.022
      },
      {
        "date": "2021-04-25",
        "daily_vaccinations": 10538,
        "daily_vaccinations_per_million": 265,
        "daily_people_vaccinated": 9283,
        "daily_people_vaccinated_per_hundred": 0.023
      },
      {
        "date": "2021-04-26",
        "daily_vaccinations": 11384,
        "daily_vaccinations_per_million": 286,
        "daily_people_vaccinated": 9711,
        "daily_people_vaccinated_per_hundred": 0.024
      },
      {
        "date": "2021-04-27",
        "daily_vaccinations": 12229,
        "daily_vaccinations_per_million": 307,
        "daily_people_vaccinated": 10138,
        "daily_people_vaccinated_per_hundred": 0.025
      },
      {
        "date": "2021-04-28",
        "daily_vaccinations": 13075,
        "daily_vaccinations_per_million": 328,
        "daily_people_vaccinated": 10566,
        "daily_people_vaccinated_per_hundred": 0.027
      },
      {
        "date": "2021-04-29",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-04-30",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-01",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-02",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-03",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-04",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-05",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-06",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-07",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-08",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-09",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-10",
        "daily_vaccinations": 13921,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-11",
        "total_vaccinations": 504502,
        "people_vaccinated": 448878,
        "people_fully_vaccinated": 55624,
        "daily_vaccinations": 13921,
        "total_vaccinations_per_hundred": 1.27,
        "people_vaccinated_per_hundred": 1.13,
        "people_fully_vaccinated_per_hundred": 0.14,
        "daily_vaccinations_per_million": 349,
        "daily_people_vaccinated": 10994,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2021-05-12",
        "daily_vaccinations": 12621,
        "daily_vaccinations_per_million": 317,
        "daily_people_vaccinated": 9764,
        "daily_people_vaccinated_per_hundred": 0.025
      },
      {
        "date": "2021-05-13",
        "daily_vaccinations": 11321,
        "daily_vaccinations_per_million": 284,
        "daily_people_vaccinated": 8534,
        "daily_people_vaccinated_per_hundred": 0.021
      },
      {
        "date": "2021-05-14",
        "daily_vaccinations": 10022,
        "daily_vaccinations_per_million": 252,
        "daily_people_vaccinated": 7304,
        "daily_people_vaccinated_per_hundred": 0.018
      },
      {
        "date": "2021-05-15",
        "daily_vaccinations": 8722,
        "daily_vaccinations_per_million": 219,
        "daily_people_vaccinated": 6074,
        "daily_people_vaccinated_per_hundred": 0.015
      },
      {
        "date": "2021-05-16",
        "daily_vaccinations": 7422,
        "daily_vaccinations_per_million": 186,
        "daily_people_vaccinated": 4844,
        "daily_people_vaccinated_per_hundred": 0.012
      },
      {
        "date": "2021-05-17",
        "daily_vaccinations": 6122,
        "daily_vaccinations_per_million": 154,
        "daily_people_vaccinated": 3615,
        "daily_people_vaccinated_per_hundred": 0.009
      },
      {
        "date": "2021-05-18",
        "daily_vaccinations": 4822,
        "daily_vaccinations_per_million": 121,
        "daily_people_vaccinated": 2385,
        "daily_people_vaccinated_per_hundred": 0.006
      },
      {
        "date": "2021-05-19",
        "daily_vaccinations": 4822,
        "daily_vaccinations_per_million": 121,
        "daily_people_vaccinated": 2385,
        "daily_people_vaccinated_per_hundred": 0.006
      },
      {
        "date": "2021-05-20",
        "total_vaccinations": 547901,
        "people_vaccinated": 470341,
        "people_fully_vaccinated": 77560,
        "daily_vaccinations": 4822,
        "total_vaccinations_per_hundred": 1.38,
        "people_vaccinated_per_hundred": 1.18,
        "people_fully_vaccinated_per_hundred": 0.19,
        "daily_vaccinations_per_million": 121,
        "daily_people_vaccinated": 2385,
        "daily_people_vaccinated_per_hundred": 0.006
      },
      {
        "date": "2021-05-21",
        "daily_vaccinations": 5040,
        "daily_vaccinations_per_million": 127,
        "daily_people_vaccinated": 2259,
        "daily_people_vaccinated_per_hundred": 0.006
      },
      {
        "date": "2021-05-22",
        "daily_vaccinations": 5257,
        "daily_vaccinations_per_million": 132,
        "daily_people_vaccinated": 2134,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-05-23",
        "daily_vaccinations": 5474,
        "daily_vaccinations_per_million": 137,
        "daily_people_vaccinated": 2008,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-05-24",
        "total_vaccinations": 573277,
        "people_vaccinated": 476367,
        "people_fully_vaccinated": 96910,
        "daily_vaccinations": 5692,
        "total_vaccinations_per_hundred": 1.44,
        "people_vaccinated_per_hundred": 1.2,
        "people_fully_vaccinated_per_hundred": 0.24,
        "daily_vaccinations_per_million": 143,
        "daily_people_vaccinated": 1883,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-05-25",
        "daily_vaccinations": 6230,
        "daily_vaccinations_per_million": 156,
        "daily_people_vaccinated": 1757,
        "daily_people_vaccinated_per_hundred": 0.004
      },
      {
        "date": "2021-05-26",
        "total_vaccinations": 590454,
        "people_vaccinated": 479372,
        "people_fully_vaccinated": 111082,
        "daily_vaccinations": 6768,
        "total_vaccinations_per_hundred": 1.48,
        "people_vaccinated_per_hundred": 1.2,
        "people_fully_vaccinated_per_hundred": 0.28,
        "daily_vaccinations_per_million": 170,
        "daily_people_vaccinated": 1631,
        "daily_people_vaccinated_per_hundred": 0.004
      },
      {
        "date": "2021-05-27",
        "total_vaccinations": 593313,
        "people_vaccinated": 479574,
        "people_fully_vaccinated": 113739,
        "daily_vaccinations_raw": 2859,
        "daily_vaccinations": 6487,
        "total_vaccinations_per_hundred": 1.49,
        "people_vaccinated_per_hundred": 1.2,
        "people_fully_vaccinated_per_hundred": 0.29,
        "daily_vaccinations_per_million": 163,
        "daily_people_vaccinated": 1319,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-05-28",
        "daily_vaccinations": 5907,
        "daily_vaccinations_per_million": 148,
        "daily_people_vaccinated": 1135,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-05-29",
        "daily_vaccinations": 5326,
        "daily_vaccinations_per_million": 134,
        "daily_people_vaccinated": 951,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-05-30",
        "total_vaccinations": 600152,
        "people_vaccinated": 480226,
        "people_fully_vaccinated": 119926,
        "daily_vaccinations": 4746,
        "total_vaccinations_per_hundred": 1.51,
        "people_vaccinated_per_hundred": 1.21,
        "people_fully_vaccinated_per_hundred": 0.3,
        "daily_vaccinations_per_million": 119,
        "daily_people_vaccinated": 766,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-05-31",
        "daily_vaccinations": 5084,
        "daily_vaccinations_per_million": 128,
        "daily_people_vaccinated": 621,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-06-01",
        "daily_vaccinations": 5102,
        "daily_vaccinations_per_million": 128,
        "daily_people_vaccinated": 476,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-02",
        "total_vaccinations": 626290,
        "people_vaccinated": 481690,
        "people_fully_vaccinated": 144600,
        "daily_vaccinations": 5119,
        "total_vaccinations_per_hundred": 1.57,
        "people_vaccinated_per_hundred": 1.21,
        "people_fully_vaccinated_per_hundred": 0.36,
        "daily_vaccinations_per_million": 129,
        "daily_people_vaccinated": 331,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-03",
        "total_vaccinations": 630305,
        "people_vaccinated": 481800,
        "people_fully_vaccinated": 148505,
        "daily_vaccinations_raw": 4015,
        "daily_vaccinations": 5285,
        "total_vaccinations_per_hundred": 1.58,
        "people_vaccinated_per_hundred": 1.21,
        "people_fully_vaccinated_per_hundred": 0.37,
        "daily_vaccinations_per_million": 133,
        "daily_people_vaccinated": 318,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-04",
        "daily_vaccinations": 5273,
        "daily_vaccinations_per_million": 132,
        "daily_people_vaccinated": 320,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-05",
        "daily_vaccinations": 5261,
        "daily_vaccinations_per_million": 132,
        "daily_people_vaccinated": 322,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-06",
        "daily_vaccinations": 5250,
        "daily_vaccinations_per_million": 132,
        "daily_people_vaccinated": 324,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-07",
        "daily_vaccinations": 4319,
        "daily_vaccinations_per_million": 108,
        "daily_people_vaccinated": 287,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-08",
        "total_vaccinations": 641295,
        "people_vaccinated": 482952,
        "people_fully_vaccinated": 158343,
        "daily_vaccinations": 3388,
        "total_vaccinations_per_hundred": 1.61,
        "people_vaccinated_per_hundred": 1.21,
        "people_fully_vaccinated_per_hundred": 0.4,
        "daily_vaccinations_per_million": 85,
        "daily_people_vaccinated": 250,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-09",
        "daily_vaccinations": 2637,
        "daily_vaccinations_per_million": 66,
        "daily_people_vaccinated": 223,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-10",
        "daily_vaccinations": 2556,
        "daily_vaccinations_per_million": 64,
        "daily_people_vaccinated": 250,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-11",
        "daily_vaccinations": 2735,
        "daily_vaccinations_per_million": 69,
        "daily_people_vaccinated": 259,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-12",
        "daily_vaccinations": 2914,
        "daily_vaccinations_per_million": 73,
        "daily_people_vaccinated": 269,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-13",
        "daily_vaccinations": 3093,
        "daily_vaccinations_per_million": 78,
        "daily_people_vaccinated": 278,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-14",
        "total_vaccinations": 662003,
        "people_vaccinated": 484737,
        "people_fully_vaccinated": 177266,
        "daily_vaccinations": 3272,
        "total_vaccinations_per_hundred": 1.66,
        "people_vaccinated_per_hundred": 1.22,
        "people_fully_vaccinated_per_hundred": 0.44,
        "daily_vaccinations_per_million": 82,
        "daily_people_vaccinated": 288,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-06-15",
        "daily_vaccinations": 4813,
        "daily_vaccinations_per_million": 121,
        "daily_people_vaccinated": 1994,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-06-16",
        "daily_vaccinations": 6175,
        "daily_vaccinations_per_million": 155,
        "daily_people_vaccinated": 3691,
        "daily_people_vaccinated_per_hundred": 0.009
      },
      {
        "date": "2021-06-17",
        "daily_vaccinations": 7538,
        "daily_vaccinations_per_million": 189,
        "daily_people_vaccinated": 5387,
        "daily_people_vaccinated_per_hundred": 0.014
      },
      {
        "date": "2021-06-18",
        "daily_vaccinations": 8900,
        "daily_vaccinations_per_million": 223,
        "daily_people_vaccinated": 7084,
        "daily_people_vaccinated_per_hundred": 0.018
      },
      {
        "date": "2021-06-19",
        "daily_vaccinations": 10262,
        "daily_vaccinations_per_million": 258,
        "daily_people_vaccinated": 8781,
        "daily_people_vaccinated_per_hundred": 0.022
      },
      {
        "date": "2021-06-20",
        "daily_vaccinations": 11624,
        "daily_vaccinations_per_million": 292,
        "daily_people_vaccinated": 10477,
        "daily_people_vaccinated_per_hundred": 0.026
      },
      {
        "date": "2021-06-21",
        "daily_vaccinations": 12986,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 12174,
        "daily_people_vaccinated_per_hundred": 0.031
      },
      {
        "date": "2021-06-22",
        "total_vaccinations": 765890,
        "people_vaccinated": 582128,
        "people_fully_vaccinated": 183762,
        "daily_vaccinations": 12986,
        "total_vaccinations_per_hundred": 1.92,
        "people_vaccinated_per_hundred": 1.46,
        "people_fully_vaccinated_per_hundred": 0.46,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 12174,
        "daily_people_vaccinated_per_hundred": 0.031
      },
      {
        "date": "2021-06-23",
        "daily_vaccinations": 13125,
        "daily_vaccinations_per_million": 329,
        "daily_people_vaccinated": 12358,
        "daily_people_vaccinated_per_hundred": 0.031
      },
      {
        "date": "2021-06-24",
        "daily_vaccinations": 13264,
        "daily_vaccinations_per_million": 333,
        "daily_people_vaccinated": 12542,
        "daily_people_vaccinated_per_hundred": 0.031
      },
      {
        "date": "2021-06-25",
        "daily_vaccinations": 13404,
        "daily_vaccinations_per_million": 336,
        "daily_people_vaccinated": 12726,
        "daily_people_vaccinated_per_hundred": 0.032
      },
      {
        "date": "2021-06-26",
        "daily_vaccinations": 13543,
        "daily_vaccinations_per_million": 340,
        "daily_people_vaccinated": 12909,
        "daily_people_vaccinated_per_hundred": 0.032
      },
      {
        "date": "2021-06-27",
        "total_vaccinations": 835694,
        "people_vaccinated": 649434,
        "people_fully_vaccinated": 186260,
        "daily_vaccinations": 13682,
        "total_vaccinations_per_hundred": 2.1,
        "people_vaccinated_per_hundred": 1.63,
        "people_fully_vaccinated_per_hundred": 0.47,
        "daily_vaccinations_per_million": 343,
        "daily_people_vaccinated": 13093,
        "daily_people_vaccinated_per_hundred": 0.033
      },
      {
        "date": "2021-06-28",
        "daily_vaccinations": 14263,
        "daily_vaccinations_per_million": 358,
        "daily_people_vaccinated": 13724,
        "daily_people_vaccinated_per_hundred": 0.034
      },
      {
        "date": "2021-06-29",
        "daily_vaccinations": 14844,
        "daily_vaccinations_per_million": 373,
        "daily_people_vaccinated": 14355,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-06-30",
        "total_vaccinations": 886854,
        "people_vaccinated": 699200,
        "people_fully_vaccinated": 187654,
        "daily_vaccinations": 15286,
        "total_vaccinations_per_hundred": 2.23,
        "people_vaccinated_per_hundred": 1.76,
        "people_fully_vaccinated_per_hundred": 0.47,
        "daily_vaccinations_per_million": 384,
        "daily_people_vaccinated": 14802,
        "daily_people_vaccinated_per_hundred": 0.037
      },
      {
        "date": "2021-07-01",
        "daily_vaccinations": 14115,
        "daily_vaccinations_per_million": 354,
        "daily_people_vaccinated": 13654,
        "daily_people_vaccinated_per_hundred": 0.034
      },
      {
        "date": "2021-07-02",
        "daily_vaccinations": 12944,
        "daily_vaccinations_per_million": 325,
        "daily_people_vaccinated": 12507,
        "daily_people_vaccinated_per_hundred": 0.031
      },
      {
        "date": "2021-07-03",
        "daily_vaccinations": 11773,
        "daily_vaccinations_per_million": 296,
        "daily_people_vaccinated": 11360,
        "daily_people_vaccinated_per_hundred": 0.029
      },
      {
        "date": "2021-07-04",
        "daily_vaccinations": 10602,
        "daily_vaccinations_per_million": 266,
        "daily_people_vaccinated": 10212,
        "daily_people_vaccinated_per_hundred": 0.026
      },
      {
        "date": "2021-07-05",
        "total_vaccinations": 915671,
        "people_vaccinated": 726349,
        "people_fully_vaccinated": 189322,
        "daily_vaccinations": 8989,
        "total_vaccinations_per_hundred": 2.3,
        "people_vaccinated_per_hundred": 1.82,
        "people_fully_vaccinated_per_hundred": 0.48,
        "daily_vaccinations_per_million": 226,
        "daily_people_vaccinated": 8618,
        "daily_people_vaccinated_per_hundred": 0.022
      },
      {
        "date": "2021-07-06",
        "daily_vaccinations": 7895,
        "daily_vaccinations_per_million": 198,
        "daily_people_vaccinated": 6881,
        "daily_people_vaccinated_per_hundred": 0.017
      },
      {
        "date": "2021-07-07",
        "total_vaccinations": 934463,
        "people_vaccinated": 735213,
        "people_fully_vaccinated": 199250,
        "daily_vaccinations": 6801,
        "total_vaccinations_per_hundred": 2.35,
        "people_vaccinated_per_hundred": 1.85,
        "people_fully_vaccinated_per_hundred": 0.5,
        "daily_vaccinations_per_million": 171,
        "daily_people_vaccinated": 5145,
        "daily_people_vaccinated_per_hundred": 0.013
      },
      {
        "date": "2021-07-08",
        "daily_vaccinations": 6965,
        "daily_vaccinations_per_million": 175,
        "daily_people_vaccinated": 4645,
        "daily_people_vaccinated_per_hundred": 0.012
      },
      {
        "date": "2021-07-09",
        "daily_vaccinations": 7128,
        "daily_vaccinations_per_million": 179,
        "daily_people_vaccinated": 4145,
        "daily_people_vaccinated_per_hundred": 0.01
      },
      {
        "date": "2021-07-10",
        "daily_vaccinations": 7292,
        "daily_vaccinations_per_million": 183,
        "daily_people_vaccinated": 3645,
        "daily_people_vaccinated_per_hundred": 0.009
      },
      {
        "date": "2021-07-11",
        "total_vaccinations": 962093,
        "people_vaccinated": 742934,
        "people_fully_vaccinated": 219159,
        "daily_vaccinations": 7455,
        "total_vaccinations_per_hundred": 2.42,
        "people_vaccinated_per_hundred": 1.87,
        "people_fully_vaccinated_per_hundred": 0.55,
        "daily_vaccinations_per_million": 187,
        "daily_people_vaccinated": 3145,
        "daily_people_vaccinated_per_hundred": 0.008
      },
      {
        "date": "2021-07-12",
        "daily_vaccinations": 9588,
        "daily_vaccinations_per_million": 241,
        "daily_people_vaccinated": 2468,
        "daily_people_vaccinated_per_hundred": 0.006
      },
      {
        "date": "2021-07-13",
        "daily_vaccinations": 11201,
        "daily_vaccinations_per_million": 281,
        "daily_people_vaccinated": 1933,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-07-14",
        "total_vaccinations": 1024168,
        "daily_vaccinations": 12815,
        "total_vaccinations_per_hundred": 2.57,
        "daily_vaccinations_per_million": 322,
        "daily_people_vaccinated": 1399,
        "daily_people_vaccinated_per_hundred": 0.004
      },
      {
        "date": "2021-07-15",
        "daily_vaccinations": 14331,
        "daily_vaccinations_per_million": 360,
        "daily_people_vaccinated": 1222,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-07-16",
        "daily_vaccinations": 15848,
        "daily_vaccinations_per_million": 398,
        "daily_people_vaccinated": 1044,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-07-17",
        "daily_vaccinations": 17364,
        "daily_vaccinations_per_million": 436,
        "daily_people_vaccinated": 867,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-18",
        "total_vaccinations": 1094257,
        "daily_vaccinations": 18881,
        "total_vaccinations_per_hundred": 2.75,
        "daily_vaccinations_per_million": 474,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-19",
        "daily_vaccinations": 17753,
        "daily_vaccinations_per_million": 446,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-20",
        "daily_vaccinations": 16626,
        "daily_vaccinations_per_million": 417,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-21",
        "daily_vaccinations": 15499,
        "daily_vaccinations_per_million": 389,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-22",
        "daily_vaccinations": 14824,
        "daily_vaccinations_per_million": 372,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-23",
        "daily_vaccinations": 14150,
        "daily_vaccinations_per_million": 355,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-24",
        "total_vaccinations": 1171064,
        "daily_vaccinations": 13476,
        "total_vaccinations_per_hundred": 2.94,
        "daily_vaccinations_per_million": 338,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-25",
        "daily_vaccinations": 11132,
        "daily_vaccinations_per_million": 279,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-26",
        "daily_vaccinations": 9464,
        "daily_vaccinations_per_million": 238,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-27",
        "daily_vaccinations": 7795,
        "daily_vaccinations_per_million": 196,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-28",
        "daily_vaccinations": 6126,
        "daily_vaccinations_per_million": 154,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-29",
        "daily_vaccinations": 4457,
        "daily_vaccinations_per_million": 112,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-30",
        "daily_vaccinations": 2788,
        "daily_vaccinations_per_million": 70,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-07-31",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-01",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-02",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-03",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-04",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-05",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-06",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-07",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-08",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-09",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-10",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-11",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-12",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-13",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-14",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-15",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-16",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-17",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-18",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-19",
        "daily_vaccinations": 1119,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-20",
        "total_vaccinations": 1201286,
        "people_vaccinated": 770542,
        "people_fully_vaccinated": 430744,
        "daily_vaccinations": 1119,
        "total_vaccinations_per_hundred": 3.02,
        "people_vaccinated_per_hundred": 1.93,
        "people_fully_vaccinated_per_hundred": 1.08,
        "daily_vaccinations_per_million": 28,
        "daily_people_vaccinated": 690,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-21",
        "daily_vaccinations": 11068,
        "daily_vaccinations_per_million": 278,
        "daily_people_vaccinated": 624,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-08-22",
        "daily_vaccinations": 21017,
        "daily_vaccinations_per_million": 528,
        "daily_people_vaccinated": 557,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-23",
        "daily_vaccinations": 30966,
        "daily_vaccinations_per_million": 777,
        "daily_people_vaccinated": 490,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-24",
        "daily_vaccinations": 40914,
        "daily_vaccinations_per_million": 1027,
        "daily_people_vaccinated": 424,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-25",
        "daily_vaccinations": 50863,
        "daily_vaccinations_per_million": 1277,
        "daily_people_vaccinated": 357,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-26",
        "daily_vaccinations": 60812,
        "daily_vaccinations_per_million": 1527,
        "daily_people_vaccinated": 290,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-27",
        "daily_vaccinations": 70761,
        "daily_vaccinations_per_million": 1776,
        "daily_people_vaccinated": 224,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-28",
        "daily_vaccinations": 70761,
        "daily_vaccinations_per_million": 1776,
        "daily_people_vaccinated": 224,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-29",
        "daily_vaccinations": 70761,
        "daily_vaccinations_per_million": 1776,
        "daily_people_vaccinated": 224,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-30",
        "daily_vaccinations": 70761,
        "daily_vaccinations_per_million": 1776,
        "daily_people_vaccinated": 224,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-08-31",
        "total_vaccinations": 1979652,
        "people_vaccinated": 773002,
        "daily_vaccinations": 70761,
        "total_vaccinations_per_hundred": 4.97,
        "people_vaccinated_per_hundred": 1.94,
        "daily_vaccinations_per_million": 1776,
        "daily_people_vaccinated": 224,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-09-01",
        "daily_vaccinations": 62509,
        "daily_vaccinations_per_million": 1569,
        "daily_people_vaccinated": 456,
        "daily_people_vaccinated_per_hundred": 0.001
      },
      {
        "date": "2021-09-02",
        "daily_vaccinations": 54257,
        "daily_vaccinations_per_million": 1362,
        "daily_people_vaccinated": 689,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-09-03",
        "daily_vaccinations": 46006,
        "daily_vaccinations_per_million": 1155,
        "daily_people_vaccinated": 922,
        "daily_people_vaccinated_per_hundred": 0.002
      },
      {
        "date": "2021-09-04",
        "daily_vaccinations": 37754,
        "daily_vaccinations_per_million": 948,
        "daily_people_vaccinated": 1155,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-09-05",
        "daily_vaccinations": 29502,
        "daily_vaccinations_per_million": 741,
        "daily_people_vaccinated": 1388,
        "daily_people_vaccinated_per_hundred": 0.003
      },
      {
        "date": "2021-09-06",
        "daily_vaccinations": 21251,
        "daily_vaccinations_per_million": 533,
        "daily_people_vaccinated": 1620,
        "daily_people_vaccinated_per_hundred": 0.004
      },
      {
        "date": "2021-09-07",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-08",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-09",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-10",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-11",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-12",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-13",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-14",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-15",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-16",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-17",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-18",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-19",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-20",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-21",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-22",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-23",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-24",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-25",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-26",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-27",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-28",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-29",
        "daily_vaccinations": 12999,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-09-30",
        "total_vaccinations": 2369625,
        "people_vaccinated": 828601,
        "daily_vaccinations": 12999,
        "total_vaccinations_per_hundred": 5.95,
        "people_vaccinated_per_hundred": 2.08,
        "daily_vaccinations_per_million": 326,
        "daily_people_vaccinated": 1853,
        "daily_people_vaccinated_per_hundred": 0.005
      },
      {
        "date": "2021-10-01",
        "daily_vaccinations": 16376,
        "daily_vaccinations_per_million": 411,
        "daily_people_vaccinated": 10165,
        "daily_people_vaccinated_per_hundred": 0.026
      },
      {
        "date": "2021-10-02",
        "daily_vaccinations": 19752,
        "daily_vaccinations_per_million": 496,
        "daily_people_vaccinated": 18477,
        "daily_people_vaccinated_per_hundred": 0.046
      },
      {
        "date": "2021-10-03",
        "daily_vaccinations": 23129,
        "daily_vaccinations_per_million": 581,
        "daily_people_vaccinated": 26788,
        "daily_people_vaccinated_per_hundred": 0.067
      },
      {
        "date": "2021-10-04",
        "daily_vaccinations": 26505,
        "daily_vaccinations_per_million": 665,
        "daily_people_vaccinated": 35100,
        "daily_people_vaccinated_per_hundred": 0.088
      },
      {
        "date": "2021-10-05",
        "daily_vaccinations": 29882,
        "daily_vaccinations_per_million": 750,
        "daily_people_vaccinated": 43412,
        "daily_people_vaccinated_per_hundred": 0.109
      },
      {
        "date": "2021-10-06",
        "daily_vaccinations": 33258,
        "daily_vaccinations_per_million": 835,
        "daily_people_vaccinated": 51723,
        "daily_people_vaccinated_per_hundred": 0.13
      },
      {
        "date": "2021-10-07",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-08",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-09",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-10",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-11",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-12",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-13",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-14",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-15",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-16",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-17",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-18",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-19",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-20",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-21",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-22",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-23",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-24",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-25",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-26",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-27",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-28",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-29",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-30",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-10-31",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-01",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-02",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-03",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-04",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-05",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-06",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-07",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-08",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-09",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-10",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-11",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-12",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-13",
        "daily_vaccinations": 36635,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-14",
        "total_vaccinations": 4018197,
        "people_vaccinated": 3530173,
        "people_fully_vaccinated": 3188609,
        "daily_vaccinations": 36635,
        "total_vaccinations_per_hundred": 10.09,
        "people_vaccinated_per_hundred": 8.86,
        "people_fully_vaccinated_per_hundred": 8.0,
        "daily_vaccinations_per_million": 920,
        "daily_people_vaccinated": 60035,
        "daily_people_vaccinated_per_hundred": 0.151
      },
      {
        "date": "2021-11-15",
        "daily_vaccinations": 33582,
        "daily_vaccinations_per_million": 843,
        "daily_people_vaccinated": 53494,
        "daily_people_vaccinated_per_hundred": 0.134
      },
      {
        "date": "2021-11-16",
        "daily_vaccinations": 30529,
        "daily_vaccinations_per_million": 766,
        "daily_people_vaccinated": 46953,
        "daily_people_vaccinated_per_hundred": 0.118
      },
      {
        "date": "2021-11-17",
        "daily_vaccinations": 27476,
        "daily_vaccinations_per_million": 690,
        "daily_people_vaccinated": 40412,
        "daily_people_vaccinated_per_hundred": 0.101
      },
      {
        "date": "2021-11-18",
        "daily_vaccinations": 24423,
        "daily_vaccinations_per_million": 613,
        "daily_people_vaccinated": 33871,
        "daily_people_vaccinated_per_hundred": 0.085
      },
      {
        "date": "2021-11-19",
        "daily_vaccinations": 21369,
        "daily_vaccinations_per_million": 536,
        "daily_people_vaccinated": 27330,
        "daily_people_vaccinated_per_hundred": 0.069
      },
      {
        "date": "2021-11-20",
        "daily_vaccinations": 18316,
        "daily_vaccinations_per_million": 460,
        "daily_people_vaccinated": 20789,
        "daily_people_vaccinated_per_hundred": 0.052
      },
      {
        "date": "2021-11-21",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-22",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-23",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-24",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-25",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-26",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-27",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-28",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-29",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-11-30",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-01",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-02",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-03",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-04",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-05",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-06",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-07",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-08",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-09",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-10",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-11",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-12",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-13",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-14",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-15",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-16",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-17",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-18",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-19",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-20",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-21",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-22",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-23",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-24",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-25",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-26",
        "daily_vaccinations": 15263,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-27",
        "total_vaccinations": 4674518,
        "people_vaccinated": 4142857,
        "people_fully_vaccinated": 3753224,
        "daily_vaccinations": 15263,
        "total_vaccinations_per_hundred": 11.73,
        "people_vaccinated_per_hundred": 10.4,
        "people_fully_vaccinated_per_hundred": 9.42,
        "daily_vaccinations_per_million": 383,
        "daily_people_vaccinated": 14248,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2021-12-28",
        "daily_vaccinations": 15598,
        "daily_vaccinations_per_million": 392,
        "daily_people_vaccinated": 14583,
        "daily_people_vaccinated_per_hundred": 0.037
      },
      {
        "date": "2021-12-29",
        "daily_vaccinations": 15932,
        "daily_vaccinations_per_million": 400,
        "daily_people_vaccinated": 14918,
        "daily_people_vaccinated_per_hundred": 0.037
      },
      {
        "date": "2021-12-30",
        "daily_vaccinations": 16266,
        "daily_vaccinations_per_million": 408,
        "daily_people_vaccinated": 15253,
        "daily_people_vaccinated_per_hundred": 0.038
      },
      {
        "date": "2021-12-31",
        "daily_vaccinations": 16601,
        "daily_vaccinations_per_million": 417,
        "daily_people_vaccinated": 15588,
        "daily_people_vaccinated_per_hundred": 0.039
      },
      {
        "date": "2022-01-01",
        "daily_vaccinations": 16935,
        "daily_vaccinations_per_million": 425,
        "daily_people_vaccinated": 15923,
        "daily_people_vaccinated_per_hundred": 0.04
      },
      {
        "date": "2022-01-02",
        "daily_vaccinations": 17269,
        "daily_vaccinations_per_million": 434,
        "daily_people_vaccinated": 16258,
        "daily_people_vaccinated_per_hundred": 0.041
      },
      {
        "date": "2022-01-03",
        "daily_vaccinations": 17604,
        "daily_vaccinations_per_million": 442,
        "daily_people_vaccinated": 16593,
        "daily_people_vaccinated_per_hundred": 0.042
      },
      {
        "date": "2022-01-04",
        "daily_vaccinations": 17604,
        "daily_vaccinations_per_million": 442,
        "daily_people_vaccinated": 16593,
        "daily_people_vaccinated_per_hundred": 0.042
      },
      {
        "date": "2022-01-05",
        "daily_vaccinations": 17604,
        "daily_vaccinations_per_million": 442,
        "daily_people_vaccinated": 16593,
        "daily_people_vaccinated_per_hundred": 0.042
      },
      {
        "date": "2022-01-06",
        "daily_vaccinations": 17604,
        "daily_vaccinations_per_million": 442,
        "daily_people_vaccinated": 16593,
        "daily_people_vaccinated_per_hundred": 0.042
      },
      {
        "date": "2022-01-07",
        "daily_vaccinations": 17604,
        "daily_vaccinations_per_million": 442,
        "daily_people_vaccinated": 16593,
        "daily_people_vaccinated_per_hundred": 0.042
      },
      {
        "date": "2022-01-08",
        "daily_vaccinations": 17604,
        "daily_vaccinations_per_million": 442,
        "daily_people_vaccinated": 16593,
        "daily_people_vaccinated_per_hundred": 0.042
      },
      {
        "date": "2022-01-09",
        "total_vaccinations": 4903368,
        "people_vaccinated": 4358570,
        "people_fully_vaccinated": 3809353,
        "daily_vaccinations": 17604,
        "total_vaccinations_per_hundred": 12.31,
        "people_vaccinated_per_hundred": 10.94,
        "people_fully_vaccinated_per_hundred": 9.56,
        "daily_vaccinations_per_million": 442,
        "daily_people_vaccinated": 16593,
        "daily_people_vaccinated_per_hundred": 0.042
      },
      {
        "date": "2022-01-10",
        "daily_vaccinations": 16642,
        "daily_vaccinations_per_million": 418,
        "daily_people_vaccinated": 15669,
        "daily_people_vaccinated_per_hundred": 0.039
      },
      {
        "date": "2022-01-11",
        "total_vaccinations": 4925111,
        "people_vaccinated": 4378818,
        "people_fully_vaccinated": 3814837,
        "daily_vaccinations": 15680,
        "total_vaccinations_per_hundred": 12.36,
        "people_vaccinated_per_hundred": 10.99,
        "people_fully_vaccinated_per_hundred": 9.58,
        "daily_vaccinations_per_million": 394,
        "daily_people_vaccinated": 14745,
        "daily_people_vaccinated_per_hundred": 0.037
      },
      {
        "date": "2022-01-12",
        "daily_vaccinations": 15287,
        "daily_vaccinations_per_million": 384,
        "daily_people_vaccinated": 14309,
        "daily_people_vaccinated_per_hundred": 0.036
      },
      {
        "date": "2022-01-13",
        "total_vaccinations": 4954809,
        "people_vaccinated": 4405897,
        "people_fully_vaccinated": 3823786,
        "daily_vaccinations": 14893,
        "total_vaccinations_per_hundred": 12.44,
        "people_vaccinated_per_hundred": 11.06,
        "people_fully_vaccinated_per_hundred": 9.6,
        "daily_vaccinations_per_million": 374,
        "daily_people_vaccinated": 13872,
        "daily_people_vaccinated_per_hundred": 0.035
      },
      {
        "date": "2022-01-14",
        "daily_vaccinations": 13458,
        "daily_vaccinations_per_million": 338,
        "daily_people_vaccinated": 12467,
        "daily_people_vaccinated_per_hundred": 0.031
      },
      {
        "date": "2022-01-15",
        "daily_vaccinations": 12023,
        "daily_vaccinations_per_million": 302,
        "daily_people_vaccinated": 11062,
        "daily_people_vaccinated_per_hundred": 0.028
      },
      {
        "date": "2022-01-16",
        "daily_vaccinations": 10588,
        "daily_vaccinations_per_million": 266,
        "daily_people_vaccinated": 9657,
        "daily_people_vaccinated_per_hundred": 0.024
      },
      {
        "date": "2022-01-17",
        "total_vaccinations": 4985047,
        "people_vaccinated": 4432926,
        "people_fully_vaccinated": 3832821,
        "daily_vaccinations": 10115,
        "total_vaccinations_per_hundred": 12.51,
        "people_vaccinated_per_hundred": 11.13,
        "people_fully_vaccinated_per_hundred": 9.62,
        "daily_vaccinations_per_million": 254,
        "daily_people_vaccinated": 9176,
        "daily_people_vaccinated_per_hundred": 0.023
      },
      {
        "date": "2022-01-18",
        "daily_vaccinations": 9920,
        "daily_vaccinations_per_million": 249,
        "daily_people_vaccinated": 8964,
        "daily_people_vaccinated_per_hundred": 0.023
      },
      {
        "date": "2022-01-19",
        "total_vaccinations": 5004050,
        "people_vaccinated": 4450211,
        "people_fully_vaccinated": 3836731,
        "daily_vaccinations": 9156,
        "total_vaccinations_per_hundred": 12.56,
        "people_vaccinated_per_hundred": 11.17,
        "people_fully_vaccinated_per_hundred": 9.63,
        "daily_vaccinations_per_million": 230,
        "daily_people_vaccinated": 8265,
        "daily_people_vaccinated_per_hundred": 0.021
      },
      {
        "date": "2022-01-20",
        "daily_vaccinations": 8235,
        "daily_vaccinations_per_million": 207,
        "daily_people_vaccinated": 7368,
        "daily_people_vaccinated_per_hundred": 0.018
      },
      {
        "date": "2022-01-21",
        "daily_vaccinations": 8355,
        "daily_vaccinations_per_million": 210,
        "daily_people_vaccinated": 7440,
        "daily_people_vaccinated_per_hundred": 0.019
      },
      {
        "date": "2022-01-22",
        "daily_vaccinations": 8475,
        "daily_vaccinations_per_million": 213,
        "daily_people_vaccinated": 7513,
        "daily_people_vaccinated_per_hundred": 0.019
      },
      {
        "date": "2022-01-23",
        "daily_vaccinations": 8595,
        "daily_vaccinations_per_million": 216,
        "daily_people_vaccinated": 7585,
        "daily_people_vaccinated_per_hundred": 0.019
      },
      {
        "date": "2022-01-24",
        "total_vaccinations": 5046054,
        "people_vaccinated": 4486527,
        "people_fully_vaccinated": 3851307,
        "daily_vaccinations": 8715,
        "total_vaccinations_per_hundred": 12.67,
        "people_vaccinated_per_hundred": 11.26,
        "people_fully_vaccinated_per_hundred": 9.67,
        "daily_vaccinations_per_million": 219,
        "daily_people_vaccinated": 7657,
        "daily_people_vaccinated_per_hundred": 0.019
      }
    ]
  }, // country ends here
]