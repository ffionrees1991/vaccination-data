package ffionvaxdata;

import java.util.List;

public class vaccination_Bean{

    // methods for setting the Strings - GETTERS & SETTERS

    /// MAIN VARIABLES ///

    // declare the attributes as Strings
    private String country;
    private String iso_code;
    private List data;


    // GETTER
    public String getCountry() {
        return country;
    }
    // SETTER
    public void setCountry(String country){
        this.country = country;
    }

    public String getIsoCode(){
        return iso_code;
    }
    public void setIsoCode(String iso_code){
        this.iso_code = iso_code;
    }

    public List getData(Integer position){
        return data;
    }
    public void setData(List data){
        this.data = data;
    }

    /// DATA VARIABLES ///

    // "date": "2021-02-22",
    // "total_vaccinations": 0,
    // "people_vaccinated": 0,
    // "total_vaccinations_per_hundred": 0.0,
    // "people_vaccinated_per_hundred": 0.0
    
    private String date;
    private Integer total_Vaccinations;
    private Integer people_Vaccinated;
    private Integer total_Vaccinations_Per_Hundred;
    private Integer people_Vaccinated_Per_Hundred;

    public String getDate(){
        return date;
    }
    public void setDate(){
        this.date = date;
    }

    
}   